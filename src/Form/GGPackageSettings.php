<?php

namespace Drupal\garden_gnome_package\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\garden_gnome_package\GGPackage;

class GGPackageSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ggpackage_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Load module's settings.
    $config = $this->config('GGN.settings');
    $default_preview = $this->configFactory->get('garden_gnome_package.settings')->get('preview');
    $default_override = false;
    $default_width = $this->configFactory->get('garden_gnome_package.settings')->get('width');
    $default_heigth = $this->configFactory->get('garden_gnome_package.settings')->get('height');
    // General Settings
    $form['general_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('General Settings'),
      // I used markup instead of description so that it goes above insted of below the textfields
      '#markup' => t("Default Player Settings"),
      '#collapsible' => FALSE,
      '#tree' => TRUE,
    );
    $form['general_settings']['size'] = array(
      '#type' => 'container',
      '#attributes' => ['style' => 'display:flex;justify-content: space-between;flex-wrap: wrap;']
    );
    $form['general_settings']['size']['width'] = array(
      '#type' => 'textfield',
      '#title' => t('width'),
      '#default_value' => $config->get('width') ? $config->get('width') : $default_width,
    );
    $form['general_settings']['size']['height'] = array(
      '#type' => 'textfield',
      '#title' => t('height'),
      '#default_value' => $config->get('height') ? $config->get('height') : $default_heigth,
    );
    $form['general_settings']['preview'] = array(
      '#type' => 'checkbox',
      '#title' => t('Start player as preview image'),
      '#description' => t('If checked, the player will start only after clicking on the preview image.'),
      '#default_value' => $config->get('preview') ? $config->get('preview') : $default_preview,
    );
    $form['general_settings']['icon'] = array(
      '#type' => 'textfield',
      '#title' => t('Preview Icon'),
      '#description' => t('Add path to your custom preview icon, if left empty it will use the default one.'),
      '#default_value' => $config->get('icon') ? $config->get('icon') : '',
    );
    $form['general_settings']['override'] = array(
      '#type' => 'checkbox',
      '#title' => t('Override package\'s settings'),
      '#description' => t('If checked it will override package\'s size settings. It won\'t affect if field view settings are overriding too.'),
      '#default_value' => $config->get('override') ? $config->get('override') : $default_override,
    );
    // Pano2VR
    $form['pano2vr'] = array(
      '#type' => 'fieldset',
      '#title' => t('Pano2VR'),
      '#collapsible' => FALSE,
      '#tree' => TRUE,
    );
    $form['pano2vr']['pano_version'] = [
      '#type' => 'select',
      '#title' => t('Player Version'),
      '#options' => $this::getPlayerOptions(GGPackage::getPanoPlayerPath()),
      '#default_value' => $config->get('pano_version'),
    ];
    // Object2VR
    $form['object2vr'] = array(
      '#type' => 'fieldset',
      '#title' => t('Object2VR'),
      '#collapsible' => FALSE,
      '#tree' => TRUE,
    );
    $form['object2vr']['object_version'] = [
      '#type' => 'select',
      '#title' => t('Player Version'),
      '#options' => $this::getPlayerOptions(GGPackage::getObjPlayerPath()),
      '#default_value' => $config->get('object_version'),
    ];
    return $form;
  }

  public static function getPlayerOptions($path)
  {
    $version_options = [];
    $version_options['package'] = t('From package');
    if(\Drupal::service('file_system')->prepareDirectory($path)){
      $scandir_result = \Drupal::service('file_system')->scanDirectory($path, '($|/.*)');
      if($scandir_result){
        foreach($scandir_result as $result)
        {
          $player_versions = explode('/', $result->uri);
          $version_options[$player_versions[1]] = $player_versions[1];
        }
      }
    }
    return $version_options;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $general_settings = $form_state->getValue('general_settings');
    // Validate that the width and height are just a number or a correct CSS size format.
    $regex_width = preg_match('/^(auto|0)$|^[+-]?[0-9]+.?([0-9]+)?(px|em|ex|%|in|cm|mm|pt|pc)$/i', $general_settings['size']['width']);
    $regex_height = preg_match('/^(auto|0)$|^[+-]?[0-9]+.?([0-9]+)?(px|em|ex|%|in|cm|mm|pt|pc)$/i', $general_settings['size']['height']);
    // As the values are nested, to reach them the '][' is needed in order to reach the subelement.
    if (!$regex_width && !is_numeric($general_settings['size']['width'])) {
      $form_state->setErrorByName('general_settings][size][width', $this->t('Width should be a valid css size value.'));
    }
    if (!$regex_height && !is_numeric($general_settings['size']['height'])) {
      $form_state->setErrorByName('general_settings][size][height', $this->t('Height should be a valid css size value.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $general_settings = $form_state->getValue('general_settings');
    $pano2vr_settings = $form_state->getValue('pano2vr');
    $object2vr_settings = $form_state->getValue('object2vr');
    $config = $this->config('GGN.settings');
    $config->set('width', $general_settings['size']['width']);
    $config->set('height', $general_settings['size']['height']);
    $config->set('preview', $general_settings['preview']);
    $config->set('icon', $general_settings['icon']);
    $config->set('override', $general_settings['override']);
    $config->set('pano_version', $pano2vr_settings['pano_version']);
    $config->set('object_version', $object2vr_settings['object_version']);
    $config->save();
    
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'GGN.settings',
    ];
  }

}
