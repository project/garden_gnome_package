<?php

namespace Drupal\garden_gnome_package\Plugin\media\Source;

use Drupal\file\FileInterface;
use Drupal\garden_gnome_package\GGPackage;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;

/**
 * External image entity media source.
 *
 * @see \Drupal\file\FileInterface
 *
 * @MediaSource(
 *   id = "ggnome_media",
 *   label = @Translation("Garden Gnome Media package"),
 *   description = @Translation("Renders garden gnome packages."),
 *   allowed_field_types = {"ggnome_file"},
 *   thumbnail_alt_metadata_attribute = "alt",
 *   default_thumbnail_filename = "no-thumbnail.png"
 * )
 */
class GGnomeMediaSource extends MediaSourceBase
{
    /**
     * Key for "Name" metadata attribute.
     *
     * @var string
     */
    const METADATA_ATTRIBUTE_NAME = 'name';

    /**
     * Key for "MIME type" metadata attribute.
     *
     * @var string
     */
    const METADATA_ATTRIBUTE_MIME = 'mimetype';

    /**
     * Key for "File size" metadata attribute.
     *
     * @var string
     */
    const METADATA_ATTRIBUTE_SIZE = 'filesize';
    /**
     * {@inheritdoc}
     */
    public function getMetadataAttributes()
    {
        return [
            static::METADATA_ATTRIBUTE_NAME => $this->t('Name'),
            static::METADATA_ATTRIBUTE_MIME => $this->t('MIME type'),
            static::METADATA_ATTRIBUTE_SIZE => $this->t('File size'),
            'width' => $this->t('Width'),
            'height' => $this->t('Height'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMetadata(MediaInterface $media, $attribute_name)
    {
        /** @var \Drupal\file\FileInterface $file */
        $file = $media->get($this->configuration['source_field'])->entity;

        // If the source field is not required, it may be empty.
        if (!$file) {
            return parent::getMetadata($media, $attribute_name);
        }
        switch ($attribute_name) {
            case static::METADATA_ATTRIBUTE_NAME:
            case 'default_name':
                return $file->getFilename();

            case static::METADATA_ATTRIBUTE_MIME:
                return $file->getMimeType();

            case static::METADATA_ATTRIBUTE_SIZE:
                return $file->getSize();

            case 'thumbnail_uri':
                $ggpkg = new GGPackage($file);
                if(!\Drupal::service('file_system')->prepareDirectory($ggpkg->package_dir)) {
                    $ggpkg->getExtractedPackage(true);
                }
                $ggpkg->parsePackage();
                $original_image = $ggpkg->package_dir . '/' . $ggpkg->preview_file;
                //load all existing styles for the preview image.
                $styles = \Drupal::entityTypeManager()->getStorage('image_style')->loadMultiple();

                // If the derivative doesn't exist yet (as the image style may have been
                // added post launch), create it.
                foreach ($styles as $style) {
                    // Get the styled image derivative.
                    $destination = $style->buildUri($original_image);
                    if (!file_exists($destination)) {
                        $style->createDerivative($original_image, $destination);
                    }
                }
                return $original_image;

            default:
                return parent::getMetadata($media, $attribute_name);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createSourceField(MediaTypeInterface $type)
    {
        return parent::createSourceField($type)->set('settings', ['file_extensions' => 'ggpkg']);
    }
}
