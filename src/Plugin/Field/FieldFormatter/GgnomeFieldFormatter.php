<?php

namespace Drupal\garden_gnome_package\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldFormatter\GenericFileFormatter;
use Drupal\garden_gnome_package\Plugin\Field\FieldWidget\GgnomeWidget;
use Drupal\garden_gnome_package\GGPackage;

/**
 * Formatter that shows the file size in a human readable way.
 *
 * @FieldFormatter(
 *   id = "ggnome_file_formatter",
 *   label = @Translation("Garden Gnome Package Formatter"),
 *   field_types = {
 *     "ggnome_file"
 *   }
 * )
 */
class GgnomeFieldFormatter extends GenericFileFormatter {
  /**
  * {@inheritdoc}
  */
 public function settingsSummary() {
   $summary = [];
   if($this->getSetting('override'))
   {
     $current_settings = 'w' . $this->getSetting('width') . ' h' . $this->getSetting('height') . ' - ' . ($this->getSetting('preview') ? $this->t('Preview') : $this->t('Embed'));
     $summary[] = $this->t('Embed settings: ') . $current_settings;
   }
   else
   {
    $summary[] = $this->t('Using module\'s settings.');
   }
   return $summary;
 }

 /**
 * {@inheritdoc}
 */
public static function defaultSettings() {
  return [
    // Create the custom settings
    'override' => false,
    'preview' => false,
    'width' => '640px',
    'height' => '480px',
    ] + parent::defaultSettings();
}


  /**
 * {@inheritdoc}
 */
public function settingsForm(array $form, FormStateInterface $form_state) {
  $form = parent::settingsForm($form, $form_state);
  $override =  $this->getSetting('override');
  $preview = $this->getSetting('preview');
  $width = $this->getSetting('width');
  $height = $this->getSetting('height');
  $form['override'] = [
    '#type' => 'checkbox',
    '#title' => $this->t('Override package\'s embed size'),
    '#default_value' => $override,
  ];
  $form['preview'] = [
    '#type' => 'checkbox',
    '#title' => $this->t('Start player as preview image'),
    '#default_value' => $preview,
  ];
  $form['width'] = [
    '#type' => 'textfield',
    '#title' => $this->t('Width of embed window'),
    '#default_value' => $width,
    '#required' => TRUE,
  ];
  $form['height'] = [
    '#type' => 'textfield',
    '#title' => $this->t('Heigth of embed window'),
    '#default_value' => $height,
    '#required' => TRUE,
  ];
  unset($form['use_description_as_link_text']);
  return $form;
}


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);    
    foreach ($elements as $delta => $file) {
      $ggpkg = new GGPackage($file['#file']);
      $ggpkg->parsePackage();
      if ($this->getSetting('override')){
        $ggpkg->use_preview = $this->getSetting('preview');
        $ggpkg->width = $this->getSetting('width');
        $ggpkg->height = $this->getSetting('height');
      }
      $ggpkg->setFieldFormatterHtml($elements, $delta);
    }
  
  return $elements;
  }

}
