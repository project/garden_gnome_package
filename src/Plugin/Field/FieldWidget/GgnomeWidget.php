<?php

namespace Drupal\garden_gnome_package\Plugin\Field\FieldWidget;

use Drupal\file\Plugin\Field\FieldWidget\FileWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Plugin implementation of the 'ggnome_file_widget' widget.
 *
 * @FieldWidget(
 *   id = "ggnome_file_widget",
 *   label = @Translation("Garden Gnome Package widget"),
 *   field_types = {
 *     "ggnome_file"
 *   }
 * )
 */
class GgnomeWidget extends FileWidget implements ContainerFactoryPluginInterface {  
}