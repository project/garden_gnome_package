<?php

namespace Drupal\garden_gnome_package\Plugin\Field\FieldType;

use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\garden_gnome_package\GGPackage;

/**
 * Plugin implementation of the 'Garden gnome package file' field type.
 *
 * @FieldType(
 *   id = "ggnome_file",
 *   label = @Translation("Garden Gnome Package Field"),
 *   description = @Translation("This field stores the ID of a ggpkg file as an integer value."),
 *   module = "garden_gnome_package",
 *   default_widget = "ggnome_file_widget",
 *   default_formatter = "ggnome_file_formatter",
 *   list_class = "\Drupal\garden_gnome_package\Plugin\Field\FieldType\GgnomeFieldItemList"
 * )
 */
class GgnomeField extends FileItem
{
    /**
     * {@inheritdoc}
     */
    public static function defaultFieldSettings()
    {
        return [
            'file_extensions' => 'ggpkg',
            'file_directory' => '[date:custom:Y]-[date:custom:m]',
            'max_filesize' => '',
            'description_field' => 0,
        ] + parent::defaultFieldSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function onChange($property_name, $notify = true)
    {
        parent::onChange($property_name, $notify);
    }

    /**
     * {@inheritdoc}
     */
    public function preSave()
    {
        try {
            $entity = $this->get('entity');
            $file = $entity->getTarget()->getValue();
            // For some reason the files were saved as temporary, need to set them as permanent.
            $file->setPermanent();
            $file->save();
            // Each time the file is saved/updated, we extract it's content.
            $ggpkg = new GGPackage($entity);
            $ggpkg->getExtractedPackage(true);
        } catch (Exception $e) {
            \Drupal::messenger()->addMessage(t('Error while extracting garden gnome package.'));
        }
        parent::preSave();
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        $entity = $this->get('entity');
        $ggpkg = new GGPackage($entity);
        if($ggpkg->deletePackage())
        {
            \Drupal::messenger()->addMessage(t('Extracted package removed.'));
        }
        else
        {
            \Drupal::messenger()->addMessage(t('Extracted package NOT removed.'));
        }
        if($ggpkg->file_info) {
            $ggpkg->file_info->delete();
        }
        parent::delete();
    }

}
