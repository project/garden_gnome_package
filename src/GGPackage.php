<?php

namespace Drupal\garden_gnome_package;

use Drupal\Core\Archiver\Zip;
use Drupal\Core\File\FileSystemInterface;

/**
 * @Plugin(
 *
 * )
 */
class GGPackage
{
    private $attachment_id;
    private $entity;
    private $module_dir;
    public $module_url;
    private $public_dir;
    public $public_url;
    public $file_info;
    public $file_url;
    public $uri;
    public $path;
    public $package_dir;
    public $package_dirname;
    public $package_path;
    public $package_url;
    public $remote_url = "";
    public $json;
    public $is_pano;
    public $is_object;
    public $xml_file;
    public $player_file;
    public $translations_file;
    public $skin_file;
    public $preview_file;
    public $pano_player_version;
    public $object_player_version;
    public $pano_player_dir;
    public $obj_player_dir;
    public $use_preview = false;
    public $preview_only = false;
    public $use_async = false;
    public $show_play_button = true;
    public $start_node = "";
    public $start_view = "";
    public $width = "500px";
    public $height = "500px";
    public $preview_icon = "";
    public $use_global_settings = false;

    public function __construct($entity = null)
    {
        if (gettype($entity) == 'object') {
            if (get_class($entity) == 'Drupal\file\Entity\File') {
                $this->file_info = $entity;
            }
        }
        if ($this->file_info === null) {
            $this->entity = $entity;
            $this->file_info = $entity->getValue();
        }
        if ($this->file_info) {
            $this->file_url = $this->file_info->createFileUrl(false);
            $this->uri = $this->file_info->getFileUri();
            $this->attachment_id = $this->file_info->fid->getValue()[0]['value'];
        }
        $default_scheme = \Drupal::config('system.file')->get('default_scheme');
        $module_handler = \Drupal::service('module_handler');
        $module_path = $module_handler->getModule('garden_gnome_package')->getPath();
        $this->module_dir = \Drupal::service('file_system')->realpath($module_path);
        $this->module_url = \Drupal::request()->getSchemeAndHttpHost() . '/' . $module_path;
        $this->public_dir = \Drupal::service('file_system')->realpath($default_scheme . "://");
        if ($wrapper = \Drupal::service('stream_wrapper_manager')->getViaUri("public://")) {
            $this->public_url = $wrapper->getExternalUrl();
        }

        if ($this->uri) {
            $path_parts = pathinfo($this->uri);
            $this->package_dirname = $path_parts['filename'];
            $this->package_dir = $path_parts['dirname'] . '/' . $path_parts['filename'];
            $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager')->getViaUri($this->package_dir);
            $this->package_url = $stream_wrapper_manager->getExternalUrl() . '/';
            $this->path = \Drupal::service('file_system')->realpath($this->uri); 
            $this->package_path = \str_replace('/', DIRECTORY_SEPARATOR, \Drupal::service('file_system')->realpath($this->package_dir));
        }
        $this->pano_player_dir = GGPackage::getPanoPlayerPath();
        $this->obj_player_dir = GGPackage::getObjPlayerPath();

        // Getting the module's configuration values
        $config = \Drupal::config('GGN.settings');
        $config_factory = \Drupal::service('config.factory');
        $this->use_global_settings = $config->get('override');
        $this->use_preview = $config->get('preview');
        $this->pano_player_version = $config->get('pano_version');
        $this->object_player_version = $config->get('object_version');
        $this->width = $config->get('width') ? $config->get('width') : '640px';
        $this->height = $config->get('height') ? $config->get('height') : '480px';
        $default_image_url = '/' . \Drupal::service('extension.path.resolver')->getPath('module', 'garden_gnome_package') . '/' . implode('/', ['images','icons', '360_arrow.png']);
        $this->preview_icon = $config->get('icon') ? $config->get('icon') : $default_image_url;
        
        // dpm($this->file_info->fid->getValue());
    }

    /**
     * Static function that gets the pano player path.
     */
    public static function getPanoPlayerPath()
    {
        $default_scheme = \Drupal::config('system.file')->get('default_scheme');
        $public_path = \Drupal::service('file_system')->realpath($default_scheme . "://");
        return $public_path . DIRECTORY_SEPARATOR . 'pano2vr_player';
    }

    /**
     * Static function that gets the object player path.
     */
    public static function getObjPlayerPath()
    {
        $default_scheme = \Drupal::config('system.file')->get('default_scheme');
        $public_path = \Drupal::service('file_system')->realpath($default_scheme . "://");
        return $public_path . DIRECTORY_SEPARATOR . 'object2vr_player';
    }

    /**
     * Extracts the package and adds the uniqueplayer var to the player file.
     * 
     * @param bool $copy_players
     * A boolean that tells if the method copyPlayers should be called.
     */
    public function getExtractedPackage($copy_players = false)
    {
        $is_new = !\Drupal::service('file_system')->prepareDirectory($this->package_dir);
        // Every time the entity is saved we check if the relative directory exists.
        if ($is_new) {
            \Drupal::service('file_system')->mkdir($this->package_dir);
            $package_file = new Zip($this->path);
            $package_file->extract($this->package_path);
        }
        $this->parsePackage();
        if ($copy_players) {
            $this->copyPlayers();
        }
        if ($is_new) {
            foreach (['pano2vr', 'object2vr'] as &$sw) {
                if (file_exists($this->package_path . DIRECTORY_SEPARATOR . $sw . "_player.js")) {
                    $uniquePlayerVar = "var " . $this->package_dirname . "_" . $sw . "Player = " . $sw . "Player;";
                    $playerFile = $this->package_path . DIRECTORY_SEPARATOR . $sw . "_player.js";
                    file_put_contents($playerFile, $uniquePlayerVar, FILE_APPEND);
                    if (file_exists($this->package_path . DIRECTORY_SEPARATOR . "skin.js")) {
                        $uniqueSkinVar = "var " . $this->package_dirname . "_" . $sw . "Skin = " . $sw . "Skin;";
                        $skinFile = $this->package_path . DIRECTORY_SEPARATOR . "skin.js";
                        file_put_contents($skinFile, $uniqueSkinVar, FILE_APPEND);
                    }
                }
            }
        }
    }

    /**
     * Copies the players to the public directory.
     */
    public function copyPlayers()
    {
        \Drupal::service('file_system')->prepareDirectory($this->pano_player_dir, FileSystemInterface::CREATE_DIRECTORY);
        \Drupal::service('file_system')->prepareDirectory($this->obj_player_dir, FileSystemInterface::CREATE_DIRECTORY);
        
        $player_version = '';
        if (!empty($this->json)) {
            if ($this->is_pano || $this->is_object) {
                if (property_exists($this->json, 'player')) {
                    if (property_exists($this->json->player, 'version')) {
                        $player_version = $this->json->player->version;
                    }
                }
            }
        }
        if ($player_version == '') {
            // try to parse older player versions from the header
            foreach (['pano2vr', 'object2vr'] as &$sw) {
                if (file_exists($this->package_path . DIRECTORY_SEPARATOR . $sw . "_player.js")) {
                    $lines = file($this->package_path . DIRECTORY_SEPARATOR . $sw . "_player.js");
                    if (isset($lines[1])) {
                        preg_match("/.*\s(\d\.\w+(\.[\w|\ ]+)?).*/", $lines[1], $matches);
                        if (isset($matches[1])) {
                            $player_version = $matches[1];
                        }
                    }
                }
            }
        }
        $player_version = str_replace(" ", "_", $player_version);
        if ($this->is_pano && $player_version != '') {
            $pano_player_version_folder = $this->pano_player_dir . DIRECTORY_SEPARATOR . $player_version;
            $pano_player_source = $this->package_path . DIRECTORY_SEPARATOR . $this->player_file;
            $pano_palyer_destination = $pano_player_version_folder . DIRECTORY_SEPARATOR . $this->player_file;
            if (!\Drupal::service('file_system')->prepareDirectory($pano_player_version_folder, FileSystemInterface::CREATE_DIRECTORY)) {
                \Drupal::service('file_system')->mkdir($pano_player_version_folder);
            }
            copy($pano_player_source, $pano_palyer_destination);
        }
        if ($this->is_object && $player_version != '') {
            $object_player_version_folder = $this->obj_player_dir . DIRECTORY_SEPARATOR . $player_version;
            $object_player_source = $this->package_path . DIRECTORY_SEPARATOR . $this->player_file;
            $object_palyer_destination = $object_player_source . DIRECTORY_SEPARATOR . $this->player_file;
            if (!\Drupal::service('file_system')->prepareDirectory($object_player_version_folder, FileSystemInterface::CREATE_DIRECTORY)) {
                \Drupal::service('file_system')->mkdir($object_player_version_folder);
            }
            copy($object_player_source, $object_palyer_destination);
        }
    }

    /**
     * Parses the package by reading the gginfo.json or, for older packages, manually checks certain files.
     */
    public function parsePackage()
    {
        $package_files = scandir($this->package_path);
        if (in_array("gginfo.json", $package_files)) {
            $gginfo_url = $this->package_dir . DIRECTORY_SEPARATOR . 'gginfo.json';
            $this->parse_gginfo_json(\file_get_contents($gginfo_url));
        } else {
            if (in_array("pano2vr_player.js", $package_files)) {
                $this->is_pano = true;
                $this->player_file = "pano2vr_player.js";
                $this->xml_file = "pano.xml";
            }
            if (in_array("object2vr_player.js", $package_files)) {
                $this->is_object = true;
                $this->player_file = "object2vr_player.js";
                $this->xml_file = "object.xml";
            }
            if (in_array("skin.js", $package_files)) {
                $this->skin_file = "skin.js";
            }
            if (in_array("translations.js", $package_files)) {
                $this->translations_file = "translations.js";
            }
            if (in_array("preview.jpg", $package_files)) {
                $this->preview_file = "preview.jpg";
            } else {
                $this->preview_file = "ggpkg.png";
            }
        }
    }

    /**
     * Parses the json file in order to set some properties of the package object.
     * 
     * @param string $json_content
     * The content of the json file in string format.
     */
    public function parse_gginfo_json($json_content)
    {
        $json = json_decode($json_content);
        if (!$json) {
            return;
        }
        $this->json = $json;
        $json_type = $json->type;
        if (isset($json_type)) {
            // only newer player version have the async function, but they where introduced at the same time as as the json file in the package
            $this->use_async = true;
            $this->is_pano = ($json_type == 'panorama');
            $this->is_object = ($json_type == 'object');
            if ($this->is_pano) {
                $this->xml_file = "pano.xml";
            }
            if ($this->is_object) {
                $this->xml_file = "object.xml";
            }
        }
        $json_player = $json->player;
        if (isset($json_player)) {
            $json_js_player = $json_player->js;
            if (isset($json_js_player)) {
                $this->player_file = $json_js_player;
            }
        }
        $json_skin = $json->skin;
        if (isset($json_skin)) {
            $json_skin_file = $json_skin->js;
            if (isset($json_skin_file)) {
                $this->skin_file = $json_skin_file;
            }
        }
        if (property_exists($json, 'translations')) {
            if (isset($json->translations)) {
                if (isset($json->translations->js)) {
                    $this->translations_file = $json->translations->js;
                }
            }
        }
        $json_preview = $json->preview;
        if (isset($json_preview)) {
            $json_preview_file = $json_preview->img;
            if (isset($json_preview_file)) {
                $this->preview_file = $json_preview_file;
            }
            if (!$this->use_global_settings) {
                if (property_exists($json->preview, 'width')) {
                    $this->width = $json->preview->width;
                }
                if (property_exists($json->preview, 'height')) {
                    $this->height = $json->preview->height;
                }
            }
        }
    }

    /**
     * Deletes the extracted package (it does not delete the orignal file).
     */
    public function deletePackage()
    {
        return $this->del_tree($this->package_path);
    }

    /**
     * Recursive function that deletes all subdirectories and files given a path.
     * 
     * @param string $dir
     * The directory to recursively delete.
     * 
     * @return bool
     * Returns true if successfully deletes the directory.
     */
    protected function del_tree($dir)
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir(implode(DIRECTORY_SEPARATOR, [$dir,$file]))) ? $this->del_tree(implode(DIRECTORY_SEPARATOR, [$dir,$file])) : unlink(implode(DIRECTORY_SEPARATOR, [$dir,$file]));
        }
        return rmdir($dir);
    }

    /**
     * generate the appropriate HTML code in order to render the package.
     * 
     * @param array $elements
     * the array containing the data of the field to be rendered.
     * 
     * @param int $delta
     * The index of the field element to be rendered.
     */
    public function setFieldFormatterHtml(&$elements, $delta)
    {
        $ID = '_' . md5(uniqid(rand(), true));
        if ($this->json) {
            $json_externals = $this->json->{'external'};
            if (isset($json_externals)) {
                $js_files = $json_externals->{'js'};
                if (isset($js_files)) {
                    $index = 0;
                    foreach ($js_files as $js_file) {
                        if (substr($js_file, 0, 4) === "http") {
                            $elements['#attached']['html_head'][] = $this->attach_script('js_' . $this->attachment_id . '_' . $index, '', $js_file);
                        } else {
                            if ((substr($js_file, 0, 6) == ('webvr/')) ||
                                (substr($js_file, 0, 6) == 'webxr/')) { // only load the webvr scripts from one source
                                $elements['#attached']['html_head'][] = $this->attach_script('js_g_' . $js_file, '', $this->package_url . $js_file);
                            } else {
                                $elements['#attached']['html_head'][] = $this->attach_script('js_' . $this->attachment_id . '_' . $index, '', $this->package_url . $js_file);
                            }
                        }
                        $index++;
                    }
                }
                $css_files = $json_externals->{'css'};
                if (isset($css_files)) {
                    $index = 0;
                    foreach ($css_files as $css_file) {
                        if (substr($css_file, 0, 4) === "http") {
                            $elements['#attached']['html_head'][] = $this->attach_css('css_' . $this->attachment_id . '_' . $index, '', $css_file);
                        } else {
                            $elements['#attached']['html_head'][] = $this->attach_css('css_' . $this->attachment_id . '_' . $index, '', $this->package_url . $css_file);
                        }
                        $index++;
                    }
                }
            }
        }

        $width = $this->width;
        $height = $this->height;
        if (is_numeric($width)) {
            $width = $width . "px";
        }
        if (is_numeric($height)) {
            $height = $height . "px";
        }
        $elements[$delta] = [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#attributes' => [
                'id' => 'ggpkg_container' . $ID,
                'style' => 'width:' . $width . '; height:' . $height . '; position: relative;',
            ],
        ];

        if ($this->use_preview) {
            $elements[$delta]['subdiv'] = [
                '#type' => 'html_tag',
                '#tag' => 'div',
                '#attributes' => [
                    'style' => 'width:100%; height:100%; overflow: hidden; position:relative;',
                ],
            ];
            if ($this->preview_file) {
                $elements[$delta]['subdiv']['preview_image'] = [
                    '#type' => 'html_tag',
                    '#tag' => 'img',
                    '#attributes' => [
                        'src' => $this->package_url . $this->preview_file,
                        'onclick' => 'startPlayer' . $ID . '();',
                        'style' => 'min-width:100%; max-width: 10000px; min-height: 100%; max-height: 10000px; position: absolute;',
                    ],
                ];
            }
            if ($this->show_play_button) {
                $elements[$delta]['subdiv']['play_button'] = [
                    '#type' => 'html_tag',
                    '#tag' => 'div',
                    '#attributes' => [
                        'onclick' => 'startPlayer' . $ID . '();',
                        'class' => 'ggnome-preview-icon',
                        'style' => "background-image: url('$this->preview_icon');",
                    ],
                ];
            }
            if (substr($width, -1) == '%') {
                if (substr($height, -1) == '%') {
                    $elements[$delta]['subdiv']['play_button']['#attributes']['style'] .= "top: 50%; margin-top: -90px; left: 50%; margin-left: -90px;";
                } else {
                    $elements[$delta]['subdiv']['play_button']['#attributes']['style'] .= "top:calc( $height / 2 - 90px); left: 50%; margin-left: -90px;";
                }
            } else {
                if (substr($height, -1) == '%') {
                    $elements[$delta]['subdiv']['play_button']['#attributes']['style'] .= "top: 50%; margin-top: - 90px; left: calc( $width / 2 - 90px);";
                } else {
                    $elements[$delta]['subdiv']['play_button']['#attributes']['style'] .= "top: calc( $height / 2 - 90px); left: calc($width / 2 - 90px);";
                }
            }
            // $elements[$delta]['subdiv']['play_button']['#attributes']['style'] .= "box-shadow: none; position: absolute;";
            $elements['#attached']['html_head'][] = $this->attach_css('css_module_style', '','/' . \Drupal::service('extension.path.resolver')->getPath('module', 'garden_gnome_package') . '/' . implode('/', ['css', 'style.css']));
        } else {
            $elements[$delta]['#value'] = "Loading...";
        }

        if ($this->remote_url == "" && $this->is_pano && $this->pano_player_version != "package" // Remote packages should load the remote player to avoid CORS trouble.
             && file_exists($this->public_dir . DIRECTORY_SEPARATOR . 'pano2vr_player' . DIRECTORY_SEPARATOR . $this->pano_player_version . DIRECTORY_SEPARATOR . "pano2vr_player.js")) {
            $elements['#attached']['html_head'][] = $this->attach_script('js_ggsw_pano2vr_player', '', $this->public_url . 'pano2vr_player' . '/' . $this->pano_player_version . '/' . "pano2vr_player.js");
        } elseif (($this->remote_url == "") && ($this->is_object) && ($this->object_player_version != "package")
            && (file_exists($this->public_dir . DIRECTORY_SEPARATOR . 'object2vr_player' . DIRECTORY_SEPARATOR . $this->object_player_version . DIRECTORY_SEPARATOR . "object2vr_player.js"))) {
            $elements['#attached']['html_head'][] = $this->attach_script('js_ggsw_object2vr_player', '', $this->public_url . 'object2vr_player' . '/' . $this->object_player_version . '/' . "object2vr_player.js");
        } else {
            $elements['#attached']['html_head'][] = $this->attach_script('js_ggsw_package_player_' . $ID, '', $this->package_url . $this->player_file);
        }
        if ($this->translations_file) {
            $elements['#attached']['html_head'][] = $this->attach_script('js_ggsw_translations_' . $ID, '', $this->package_url . $this->translations_file);
        }
        if ($this->skin_file) {
            $elements['#attached']['html_head'][] = $this->attach_script('js_ggsw_skin_' . $ID, '', $this->package_url . $this->skin_file);
        }

        $script_wrapper = '';
        $uplayer = $this->package_dirname;
        if (is_numeric(substr($uplayer, 0, 1))) {
            $uplayer = '_' . $uplayer;
        }
        if ($this->is_object) {
            $jsObj = "obj" . $ID;
            $jsClassPrefix = "object2vr";
        } else {
            $jsObj = "pano" . $ID;
            $jsClassPrefix = "pano2vr";
        }
        if ($uplayer != "") {
            $play_js = "if  (typeof " . $uplayer . "_" . $jsClassPrefix . "Player != 'undefined') {\n";
            $play_js .= "\t" . $jsObj . "=new " . $uplayer . "_" . $jsClassPrefix . "Player('ggpkg_container" . $ID . "');\n";
            $play_js .= "} else {\n";
            $play_js .= "\t" . $jsObj . "=new " . $jsClassPrefix . "Player('ggpkg_container" . $ID . "');\n";
            $play_js .= "}\n";
        } else {
            $play_js = $jsObj . "=new " . $jsClassPrefix . "Player('ggpkg_container" . $ID . "');\n";
        }
        if ($this->start_node != "") {
            $play_js .= $jsObj . ".startNode = '" . $this->start_node . "';\n";
        }
        if ($this->start_view != "") {
            $view_params = explode(DIRECTORY_SEPARATOR, $this->start_view, 4);
            if (sizeof($view_params) >= 3) {
                $play_js .= $jsObj . ".startView = {\n";
                $play_js .= "   pan :" . floatval(trim($view_params[0])) . ",\n";
                $play_js .= "   tilt :" . floatval(trim($view_params[1])) . ",\n";
                $play_js .= "   fov :" . floatval(trim($view_params[2])) . ",\n";
                if (sizeof($view_params) > 3) {
                    $play_js .= "   projection :" . intval(trim($view_params[3])) . ",\n";
                }
                $play_js .= "};\n";
            }
        }
        if ($this->skin_file) {
            if ($uplayer != "") {
                $play_js .= "if  (typeof " . $uplayer . "_" . $jsClassPrefix . "Skin != 'undefined') {\n";
                $play_js .= "\tskin" . $ID . "=new " . $uplayer . "_" . $jsClassPrefix . "Skin(" . $jsObj . ", '" . $this->package_url . "');\n";
                $play_js .= "} else {\n";
                $play_js .= "\tskin" . $ID . "=new " . $jsClassPrefix . "Skin(" . $jsObj . ", '" . $this->package_url . "');\n";
                $play_js .= "}\n";
            } else {
                $play_js .= "skin" . $ID . "=new " . $jsClassPrefix . "Skin(" . $jsObj . ", '" . $this->package_url . "');\n";
            }
        }
        if ($this->use_async) {
            $play_js .= $jsObj . ".readConfigUrlAsync('" . $this->package_url . $this->xml_file . "');\n";
        } else {
            $play_js .= $jsObj . ".readConfigUrl('" . $this->package_url . $this->xml_file . "');\n";
        }
        $play_js .= "var contentOverflow = document.getElementById('content') ? document.getElementById('content').style.overflow : '';\n";
        
        if ($this->use_preview) {
            if (!$this->preview_only) {
                $script_wrapper .= "function startPlayer" . $ID . "() {\n$play_js\n}\n";
            }
        } else {
            $script_wrapper .= "window.addEventListener('load',function() {\n$play_js\n});\n";
        }

        $elements['#attached']['html_head'][] = $this->attach_script('ggpkg_viewer_inline_script' . $ID, $script_wrapper);

        return $elements;
    }

    /**
     * Helper function to attach the script onto the page that renders the field.
     * 
     * @param string $key
     * A key identifier of the script attached.
     * 
     * @param string $inline_script
     * The inline script to be attached.
     * 
     * @param string $src
     * The url of the script.
     */
    private function attach_script($key, $inline_script, $src = '')
    {
        $script = [
            // The data.
            [
                '#type' => 'html_tag',
                // The HTML tag to add, in this case a  tag.
                '#tag' => 'script',
                // The value of the HTML tag
                '#value' => $inline_script,
                // Set attributes like src to load a file.
                '#attributes' => array('type' => 'text/javascript'),

            ],
            // A key, to make it possible to recognize this HTML  element when altering.
            $key,
        ];
        if ($src !== '') {
            $script[0]['#attributes']['src'] = $src;
        }
        return $script;
    }


    /**
     * Helper function to attach css style onto the page that renders the field.
     * 
     * @param string $key
     * A key identifier of the css attached.
     * 
     * @param string $inline_script
     * The inline css to be attached.
     * 
     * @param string $src
     * The url of the css.
     */
    private function attach_css($key, $inline_css, $src = '')
    {
        $css_tag = [
            // The data.
            [
                '#type' => 'html_tag',
                // The HTML tag to add, in this case a  tag.
                '#tag' => $inline_css ? 'style' : 'link',
                // The value of the HTML tag
                '#value' => $inline_css,
                // Set attributes like src to load a file.
                '#attributes' => [
                    'media' => 'all',
                    'rel' => 'stylesheet'
                ],
            ],
            // A key, to make it possible to recognize this HTML  element when altering.
            $key,
        ];
        if ($src) {
            $css_tag[0]['#attributes']['href'] = $src; 
        }
        
        return $css_tag;
    }

}
